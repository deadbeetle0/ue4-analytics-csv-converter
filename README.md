# Description

Converts analytic JSON to human-readable CSV spreadsheets separated by event
type.

# Usage

Place Unreal's *.analytics files in a folder called `input` in the same
directory as the script, then invoke the python interpreter on the script. 
