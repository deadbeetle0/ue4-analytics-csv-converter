# Copyright 2020 Anton Kossler
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import csv
import os
import errno
import os.path

# A function to get the attribute 
def getAttribNames(event):
    filename = "output/" + event["eventName"] + ".csv"
    attribnames = []
    # If there is already a matching CSV table, get the headers
    # from that...
    if os.path.isfile(filename):
        with open(filename, "r") as file:
            reader = csv.DictReader(file)
            # The first two fields are going to be added back in
            # later, so I slice them off here
            attribnames = reader.fieldnames[2:]

    # ...otherwise, grab them from the attributes of the event
    # and write them to a new CSV table
    if not attribnames:
        attribnames = [attrib["name"] for attrib in event["attributes"]]
        with open(filename, "w") as file:
            writer = csv.DictWriter(
                file,
                # The default line terminator is CRLF, which
                # creates empty rows on Windows. Gross...
                lineterminator="\n",
                # Add the first two columns back in
                fieldnames=["SessionID", "UserID"] + attribnames
            )
            writer.writeheader()
    return ["SessionID", "UserID"] + attribnames

def createWriters(events):
    writers = dict()
    outfiles = dict()
    for event in data["events"]:
        eventname = event["eventName"]
        # Avoid recalculating attributes for an event type we've seen before
        if eventname in outfiles:
            continue
        filename = "output/" + eventname + ".csv"
        attribnames = getAttribNames(event)
        outfiles[eventname] = open(filename, "a")
        writers[eventname] = csv.DictWriter(
            outfiles[eventname],
            lineterminator="\n",
            fieldnames=attribnames,
            # If, for some reason, an event doesn't have all of its attributes,
            # leave the cell blank
            restval=""
        )
    return (writers, outfiles)


# Create the output directory, if it doesn't already exist
try:
    os.makedirs("output")
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

manifest = []
# Create the manifest file, if it doesn't already exist
with open("output/Manifest.txt", "a") as file:
    pass

with open("output/Manifest.txt", "r") as manifest_file:
    manifest = [line.strip() for line in manifest_file.readlines()]
    # Parse only new files in the input directory
    for filename in os.listdir("./input"):
        if not filename in manifest:
            manifest.append(filename)
            with open("input/" + filename) as file:
                data = json.load(file)
                sessionID = data["sessionId"]
                userID = data["userId"]
                writers, outfiles = createWriters(data["events"])
                for event in data["events"]:
                    # Convert the name-value pair from the attribute to a python
                    # dictionary
                    attribs = {
                        attrib["name"] : attrib["value"]
                        for attrib in event["attributes"]
                    }
                    writers[event["eventName"]].writerow(dict(
                        {"SessionID": sessionID, "UserID": userID}, **attribs
                    ))

with open("output/Manifest.txt", "w") as manifest_file:
    [manifest_file.write("%s\n" % x) for x in manifest]

# Garbage collector/operating system will close all those open files for us :)